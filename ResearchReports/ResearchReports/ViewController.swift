//
//  ViewController.swift
//  ResearchReports
//
//  Created by Taylor, Andrew on 07/11/2018.
//  Copyright © 2018 Taylor, Andrew. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController{
    
    @IBOutlet weak var titleField: UITextField!  // Outlets for details screen
    @IBOutlet weak var IDView: UITextField!
    @IBOutlet weak var authorField: UITextField!
    @IBOutlet weak var ownerView: UITextField!
    @IBOutlet weak var modifiedView: UITextField!
    @IBOutlet weak var commentView: UITextField!
    @IBOutlet weak var abstractView: UITextView!
    
    @IBOutlet weak var outletLoad: UIButton!
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var switchOutlet: UISwitch!
    
    
    var currentTitle = "" // Local variables to the specific detail view
    var currentAuthor = ""
    var currentID = ""
    var currentOwner = ""
    var currentAbstract = NSMutableAttributedString() // As details appear to be passed as with HTML tags
    var currentComment = ""
    var currentModified = ""
    var currentURL = URL(string: "")
    var currentYear = ""
    var currentFav: Bool?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleField.text = currentTitle // Set the fields to the local variables
        authorField.text = currentAuthor
        IDView.text = currentID
        ownerView.text = currentOwner
        abstractView.attributedText = currentAbstract
        commentView.text = currentComment
        modifiedView.text = currentModified
        
        if(currentURL == nil){
            outletLoad.isHidden = true // Hides favourite button if no URL is present
        }
        
        if(self.currentFav == true){
            switchOutlet.isOn = true // If report is a favourite set switch to on
        }
 
    }

    @IBAction func loadReport(_ sender: Any) {
        UIApplication.shared.open(currentURL!, options: [:]) // Open URL
    }
    
    @IBAction func addFav(_ sender: Any) {
        if(currentFav != true){
            
            print("trying to create fav") // Set as a favouritem store in array and core data
            let createFavourite = NSManagedObject(entity: entity!, insertInto: context)
            createFavourite.setValue(currentID, forKey: "id")
            createFavourite.setValue(currentYear, forKey: "year")
            
            let newFavourite = favourites(id: currentID, year: currentYear)
            favArray.append(newFavourite)
            currentFav = true
            
            do {
                try context.save()
            } catch {
                print("save to core data failed")
            }
        }
        
        else if (currentFav == true){ // If already favourite remove from array and CoreData
            
            print("Trying to remove favourite")
            let fetchRequest: NSFetchRequest<Favourites> = Favourites.fetchRequest()
                fetchRequest.predicate = NSPredicate.init(format: "year==\(currentYear) AND id==\(currentID)")
                if let result = try? context.fetch(fetchRequest) {
                    for object in result {
                        print(object)
                        context.delete(object)
                    }
                }
            currentFav = false
            }
        }
    
}

