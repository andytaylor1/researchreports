//
//  GetReports.swift
//  ResearchReports
//
//  Created by Taylor, Andrew on 07/11/2018.
//  Copyright © 2018 Taylor, Andrew. All rights reserved.
//

import UIKit
import CoreData

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let context = appDelegate.persistentContainer.viewContext

let entity = NSEntityDescription.entity(forEntityName: "Favourites", in: context)


class Reports: NSManagedObject {
    @NSManaged var id: String
    @NSManaged var year: String
    
}
struct technicalReports: Decodable {
    var techreports: [techReport]
    
}


struct techReport: Decodable {
    let year: String
    let id: String
    let owner: String?
    let authors: String
    let title: String?
    let abstract: String?
    let pdf: URL?
    let comment: String?
    let lastModified: String
    var favourite: Bool?
    
}

struct favourites {
    var id : String
    var year: String
}
