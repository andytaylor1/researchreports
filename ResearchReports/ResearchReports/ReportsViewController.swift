//
//  ReportsViewController.swift
//  ResearchReports
//
//  Created by Taylor, Andrew on 07/11/2018.
//  Copyright © 2018 Taylor, Andrew. All rights reserved.
//

import UIKit
import CoreData

// Stores the reports in a String key of Year and report object
struct Objects {
    
    var sectionName : String!
    var sectionObjects : [techReport]!
}


// Global array of favourites to be compared against
var favArray = [favourites]()

    // stores current row and
    var sectionNumber = -1
    var rowNumber = -1

    // Stores array of reports
var objectArray = [Objects]()

class ReportsViewController: UITableViewController {
    
    @IBOutlet var table: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Pull core data
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Favourites")
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let f = favourites(id: data.value(forKey: "id") as! String, year: data.value(forKey: "year") as! String)
                favArray.append(f)
            }
        } catch {
            print("core data acquisition failed")
        }
        
        if let url = URL(string: "https://cgi.csc.liv.ac.uk/~phil/Teaching/COMP327/techreports/data.php?class=techreports"){
            
        let session = URLSession.shared
            session.dataTask(with: url) { (data, response, err) in
            
            guard let jsonData = data else { return }
            
            do{
                let decoder = JSONDecoder()
                let reportList = try decoder.decode(technicalReports.self, from: jsonData)
                let yearGroups = Dictionary(grouping: reportList.techreports, by: {$0.year}) // Store results from JSON in dictionary
                let yearGroupsSorted = yearGroups.sorted(by: {$0.key > $1.key}) // Sort dictionary by year
                    for (key, value) in yearGroupsSorted {
                        objectArray.append(Objects(sectionName: key, sectionObjects: value)) // Append sorted dictionary to the object array
                    }
                DispatchQueue.main.async{
                    for currentFav in favArray { // Check for favourites and set boolean true
                        var x = 0
                        for currentYear in objectArray  {
                            if (currentYear.sectionName == currentFav.year){
                                var y = 0
                                for currentReport in objectArray[x].sectionObjects {
                                    if (currentFav.id == currentReport.id){
                                        objectArray[x].sectionObjects[y].favourite = true
                                    }
                                    y += 1
                                }
                            
                            }
                            x += 1
                        }
                    }
                    self.table.reloadData()
                }
                
            } catch let jsonErr {
                print("Error decoding JSON", jsonErr)
            }
        }.resume()
            
        }
        
    }

    
    override func viewDidAppear(_ animated: Bool) {
            for currentFav in favArray { // Check for favourites again
                var x = 0
                for currentYear in objectArray  {
                    if (currentYear.sectionName == currentFav.year){
                        var y = 0
                        for currentReport in objectArray[x].sectionObjects {
                            if (currentFav.id == currentReport.id){
                                objectArray[x].sectionObjects[y].favourite = true
                            }
                            y += 1
                        }
                        
                    }
                    x += 1
                }
            }
        self.table.reloadData()
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return objectArray.count // Number of sections = number of years
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return objectArray[section].sectionObjects.count // NUmber of rows = number of reports in that year
    }


    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCell.CellStyle.subtitle, reuseIdentifier:"cell")
        
        if(objectArray[indexPath.section].sectionObjects[indexPath.row].favourite == true){ // Set checkmark if the favourite boolean is ttue
            cell.accessoryType = .checkmark
        }
        cell.textLabel?.text =  objectArray[indexPath.section].sectionObjects[indexPath.row].title ?? "No title Specified" // Set the title of cell
        cell.detailTextLabel?.text = objectArray[indexPath.section].sectionObjects[indexPath.row].authors // Set the subtitle of cell

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        rowNumber = indexPath.row // Set the column index
        sectionNumber = indexPath.section // Set the row index
        performSegue(withIdentifier: "to Details", sender: nil) // Go to details view
    }

    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return objectArray[section].sectionName // Set sectiion name to year key
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "to Details" { // Set the local variables of selected details view
            let secondViewController = segue.destination as! ViewController
            secondViewController.currentTitle = objectArray[sectionNumber].sectionObjects[rowNumber].title ?? "No title specified"
            secondViewController.currentAuthor = objectArray[sectionNumber].sectionObjects[rowNumber].authors
            secondViewController.currentID = objectArray[sectionNumber].sectionObjects[rowNumber].id
            secondViewController.currentOwner = objectArray[sectionNumber].sectionObjects[rowNumber].owner ?? "no current owner"
            secondViewController.currentAbstract = NSMutableAttributedString(string: objectArray[sectionNumber].sectionObjects[rowNumber].abstract ?? "No abstract available")
            secondViewController.currentComment = objectArray[sectionNumber].sectionObjects[rowNumber].comment ?? "No Comments"
            secondViewController.currentModified = objectArray[sectionNumber].sectionObjects[rowNumber].lastModified
            secondViewController.currentURL = objectArray[sectionNumber].sectionObjects[rowNumber].pdf
            secondViewController.currentYear = objectArray[sectionNumber].sectionObjects[rowNumber].year
            secondViewController.currentFav = objectArray[sectionNumber].sectionObjects[rowNumber].favourite
            
            
        }
    }
}
