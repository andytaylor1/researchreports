//
//  favouriteCoreProperties.swift
//  ResearchReports
//
//  Created by Taylor, Andrew on 09/11/2018.
//  Copyright © 2018 Taylor, Andrew. All rights reserved.
//

import Foundation
import CoreData


extension  favourites {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<favourites> {
        return NSFetchRequest<favourites>(entityName: "favourites")
    }
    
    @NSManaged public var id: String?
    @NSManaged public var year: String?
    
}
